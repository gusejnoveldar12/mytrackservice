var admin = require('firebase-admin');

var serviceAccount=require("../auth/service-account-file.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'your database url'
});

module.exports = admin;
