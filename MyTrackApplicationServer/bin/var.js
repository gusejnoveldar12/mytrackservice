const root_path = 'your root path';

function date_to_string(datetime) {
  var dt = new Date(datetime);
  dt = dt.getFullYear()+'.'+
      ((1+dt.getMonth()<10)?'0'+(1+dt.getMonth()):(1+dt.getMonth()))+'.'+
      ((dt.getDate()<10)?'0'+(dt.getDate()):(dt.getDate()))+' в '+
      ((dt.getHours()<10)?'0'+(dt.getHours()):(dt.getHours()))+':'+
      ((dt.getMinutes()<10)?'0'+(dt.getMinutes()):(dt.getMinutes()))+':'+
      ((dt.getSeconds()<10)?'0'+(dt.getSeconds()):(dt.getSeconds()));
  return dt;
}

module.exports.root_path = root_path;
module.exports.date_to_string = date_to_string;