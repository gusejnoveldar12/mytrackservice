package com.example.mytrack;


import android.content.Context;
import android.util.Log;


import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;

public final class MyWebSocketListener extends WebSocketListener {
    public static final int NORMAL_CLOSURE_STATUS = 1000;
    Context cont;

    MyWebSocketListener(Context context) {
        super();
        this.cont = context;
    }

    interface onStringMessageCallBack {
        void StringMessageCallBackReturn (String s);
    }

    onStringMessageCallBack stringMessageCallBack;

    void registerStringMessageCallBack(onStringMessageCallBack callBack) {
        this.stringMessageCallBack = callBack;
    }

    @Override
    public void onOpen(WebSocket webSocket, Response response) {
        /*webSocket.send("Hello, it's SSaurel !");
        webSocket.send("What's up ?");
        webSocket.send(ByteString.decodeHex("deadbeef"));
        webSocket.close(NORMAL_CLOSURE_STATUS, "Goodbye !");*/
    }
    @Override
    public void onMessage(WebSocket webSocket, String text) {
        Log.d("Receiving : ", text);
        //Toast.makeText(cont, text, Toast.LENGTH_LONG);
        stringMessageCallBack.StringMessageCallBackReturn(text);
    }
    @Override
    public void onMessage(WebSocket webSocket, ByteString bytes) {
        Log.d("Receiving bytes : ", bytes.hex());
    }
    @Override
    public void onClosing(WebSocket webSocket, int code, String reason) {
        webSocket.close(NORMAL_CLOSURE_STATUS, null);
        Log.d("Closing : " + code + " / ", reason);
    }
    @Override
    public void onFailure(WebSocket webSocket, Throwable t, Response response) {
        Log.d("Error : ", t.getMessage());
    }
}