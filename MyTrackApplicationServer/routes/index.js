var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'MyTrack' });
});
/* GET apk. */
router.get('/apk', function(req, res, next) {
  res.download('./public/files/mytrack.apk');
});

module.exports = router;
