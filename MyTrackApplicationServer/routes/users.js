var database = require('../sqlite3/database');
var express = require('express');
var router = express.Router();
var date_to_string = (require('../bin/var')).date_to_string;
const root_path = (require('../bin/var')).root_path;

/* GET users listing. */
router.get('/*', async function(req, res, next) {
  var link = req.path.slice(1);
  if (await database.is_link(link)) {
    var row = await database.get_row_by_link(link);
    var ws_link = 'wss'+root_path.slice(5)+'/users/'+link;
    res.render('maps', { title: row.email, lat: row.lat, lng: row.lng, name: row.name, datetime: row.datetime, ws_link: ws_link });
  }
  else {
    res.status(404).render('custom_error', { message: 'Страница не найдена', status: 404});
  }
});

module.exports = router;
