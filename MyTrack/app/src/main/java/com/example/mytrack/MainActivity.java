package com.example.mytrack;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;

import java.util.Arrays;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.WebSocket;

public class MainActivity extends AppCompatActivity {

    public static final int FINE_LOCATION_PERMISSION_CODE = 2;
    private static final int REQUEST_CODE = 35;
    List<AuthUI.IdpConfig> providers;
    Button btnSignOut, btnGetURL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        btnGetURL = (Button)findViewById(R.id.btnCopyURL);
        btnGetURL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().getCurrentUser().getIdToken(true).addOnCompleteListener(new getTokenAndReceiveLinkComplete(v.getContext()));
            }
        });

        btnSignOut = (Button)findViewById(R.id.btnSignOut);
        btnSignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Logout
                AuthUI.getInstance()
                        .signOut(MainActivity.this)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                btnGetURL.setEnabled(false);
                                btnSignOut.setEnabled(false);
                                stopTrackConnection();
                                showSignInOptions();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(MainActivity.this, "" + e.getMessage(), Toast.LENGTH_LONG);
                    }
                });

            }
        });

        //init provider
        providers = Arrays.asList(
                new AuthUI.IdpConfig.EmailBuilder().build()
        );

        checkSignIn();
    }

    private void showSignInOptions() {
        startActivityForResult(
                AuthUI.getInstance().createSignInIntentBuilder()
                .setAvailableProviders(providers)
                .setTheme(R.style.MyTheme)
                .build(), REQUEST_CODE
        );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {

            IdpResponse response = IdpResponse.fromResultIntent(data);
            if (resultCode == RESULT_OK) {

                 //Get User
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                checkPermission(Manifest.permission.ACCESS_FINE_LOCATION, FINE_LOCATION_PERMISSION_CODE);


                Toast.makeText(this, user.getEmail().toString(), Toast.LENGTH_LONG).show();
                btnSignOut.setEnabled(true);
                btnGetURL.setEnabled(true);
            }
            else {
                Toast.makeText(this, response.getError().getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public void startTrackConnetion() {
        if (!isMyServiceRunning(TrackConnection.class))
            startService(new Intent(this, TrackConnection.class));
    }

    public void stopTrackConnection() {

        Intent alarmai = new Intent("START_TRACK_CONNECTION");
        alarmai.setComponent(new ComponentName(this, TrackConnectionLauncher.class));
        PendingIntent alarmpai = PendingIntent.getBroadcast(this, 1, alarmai, 0);
        AlarmManager mAlarmManager = (AlarmManager)getSystemService(this.getApplicationContext().ALARM_SERVICE);
        mAlarmManager.cancel(alarmpai);
        stopService(new Intent(this, TrackConnection.class));
    }

    protected void checkSignIn() {
        super.onStart();
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

        if (currentUser == null) {
            showSignInOptions();
        }
        else {
            checkPermission(Manifest.permission.ACCESS_FINE_LOCATION, FINE_LOCATION_PERMISSION_CODE);
            btnSignOut.setEnabled(true);
            btnGetURL.setEnabled(true);
            Toast.makeText(this, currentUser.getDisplayName(), Toast.LENGTH_LONG).show();

        }
    }

    public void checkPermission(String permission, int requestCode) {
        if (ContextCompat.checkSelfPermission(MainActivity.this, permission) == PackageManager.PERMISSION_DENIED) {
            if (shouldShowRequestPermissionRationale(permission)) {
                new AlertDialog.Builder(this)
                        .setTitle("Требуется предоставить доступ к геолокации")
                        .setMessage("Для работы трекера нужно разрешить использовать GPS")
                        .setPositiveButton("Хорошо", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(MainActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        FINE_LOCATION_PERMISSION_CODE);
                            }
                        })
                        .create()
                        .show();
            } else {
                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        FINE_LOCATION_PERMISSION_CODE);
            }
        } else {
            if (!isMyServiceRunning(TrackConnection.class)) {
                startTrackConnetion();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case FINE_LOCATION_PERMISSION_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        if (!isMyServiceRunning(TrackConnection.class)) {
                            startTrackConnetion();
                        }
                    }

                } else {
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            FINE_LOCATION_PERMISSION_CODE);

                }
                return;
            }
        }
    }

    class getTokenAndReceiveLinkComplete implements OnCompleteListener<GetTokenResult>, MyWebSocketListener.onStringMessageCallBack {
        Context context;
        WifiManager mWifiManager;
        WifiManager.WifiLock mWifiManagerLock;

        getTokenAndReceiveLinkComplete(Context context) {
            this.context = context;
        }

        @Override
        public void StringMessageCallBackReturn(final String s) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ClipboardManager clipboard = (ClipboardManager)getSystemService(MainActivity.this.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("LINK", s);
                    clipboard.setPrimaryClip(clip);
                    Toast.makeText(MainActivity.this, "Ссылка на страницу с геопозицией данного устройства скопирована в буфер обмена", Toast.LENGTH_LONG).show();
                }
            });

        }

        @Override
        public void onComplete(@NonNull Task<GetTokenResult> task) {
            if (task.isSuccessful()) {
                Request request = new Request.Builder().url("wss://MyTrackApplication--a4elru16.repl.co/api").build();
                MyWebSocketListener myWebSocketListener = new MyWebSocketListener(context);
                myWebSocketListener.registerStringMessageCallBack(this);
                OkHttpClient client = new OkHttpClient();
                WebSocket ws = client.newWebSocket(request, myWebSocketListener);
                client.dispatcher().executorService().shutdown();

                String token = task.getResult().getToken();

                mWifiManager = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);;
                mWifiManagerLock = null;

                mWifiManagerLock = mWifiManager.createWifiLock(WifiManager.WIFI_MODE_FULL, "MyWFL");
                mWifiManagerLock.acquire();

                ws.send("[\"" + token + "\", \"GETLINK\"]");

                //ws.close(MyWebSocketListener.NORMAL_CLOSURE_STATUS, "GOODBYE");
                mWifiManagerLock.release();
            }
        }
    }

}


