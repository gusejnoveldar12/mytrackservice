const sqlite3 = require('sqlite3').verbose();
var my_math = require('../math/my_math');
const root_path = (require('../bin/var')).root_path;
const date_to_string = (require('../bin/var')).date_to_string;
// Connect
let db = new sqlite3.Database('./sqlite3/database.db', (err) => {
  if (err) {
    return console.error(err.message);
  }
  // console.log('Connected to SQlite database.');
});
// Create table and insert demo
db.run(`CREATE TABLE IF NOT EXISTS locations(
                                      uid TEXT PRIMARY KEY,
                                      link TEXT UNIQUE,
                                      lat REAL,
                                      lng REAL,
                                      datetime INTEGER,
                                      name TEXT,
                                      email TEXT UNIQUE)`, [], (err) => {
  if (err) {
    return console.error(err.message);
  }
  // console.log('Create table is not exists.');
});
// Functions
function insert_location(uid, link, lat, lng, name, email) {
  db.run(`INSERT INTO locations(uid, link, lat, lng, datetime, name, email) VALUES(?,?,?,?,?,?,?)`,
        [uid, link, lat, lng, Date.now(), name, email], (err) => {
    if (err) {
      return console.log(err.message);
    }
    console.log(`${date_to_string(Date.now())} INSERT location UID(${uid}) link(${link}) lat(${lat}) lng(${lng}) name(${name}) email(${email})`);
  });
}
function update_location_pos(uid, lat, lng) {
  db.run(`UPDATE locations SET lat=?, lng=?, datetime=? WHERE uid=?`,
        [lat, lng, Date.now(), uid], (err) => {
    if (err) {
      return console.log(err.message);
    }
    console.log(`${date_to_string(Date.now())} UPDATE(POS) location UID(${uid}) lat(${lat}) lng(${lng})`);
  });
}
function update_location_link(uid, link) {
  db.run(`UPDATE locations SET link=? WHERE uid=?`,
        [link, uid], (err) => {
    if (err) {
      return console.log(err.message);
    }
    console.log(`A row has been updated (link) with UID ${uid}`);
  });
}

async function is_link(link) {
  return await new Promise((resolve, reject) => {
    db.get(`SELECT    link
                      FROM locations
                      WHERE link = ?`, [link], (err, row) => {
      if (err) {
        reject(err);
        return console.error(err.message);
      }
      if (typeof row === 'undefined') {
        // console.log(`is_link(${link}): NOT FOUND`);
        resolve(false);
        return false;
      } else {
        // console.log(`is_link(${row.link}): OK`);
        resolve(true);
        return true;
      }
    });
  });
}
async function uid_is_exists(uid) {
  return await new Promise((resolve, reject) => {
    db.get(`SELECT    uid
                      FROM locations
                      WHERE uid = ?`, [uid], (err, row) => {
      if (err) {
        reject(err);
        return console.error(err.message);
      }
      if (typeof row === 'undefined') {
        // console.log(`uid_is_exists(${uid}): NOT FOUND`);
        resolve(false);
        return false;
      } else {
        // console.log(`uid_is_exists(${row.uid}): OK`);
        resolve(true);
        return true;
      }
    });
  });
}

async function new_link() {
  return await new Promise(async (resolve, reject) => {
    var link = my_math.get_random_link();
    while (await is_link(link)) {
      link = my_math.get_random_link();
    }
    resolve(link);
  });
}
async function get_full_link_by_uid(uid) {
  return await new Promise((resolve, reject) => {
    db.get(`SELECT    link
                      FROM locations
                      WHERE uid = ?`, [uid], (err, row) => {
      if (err) {
        reject(err);
        return console.error(err.message);
      }
      if (typeof row === 'undefined') {
        console.log(`get_link_by_uid: NOT FOUND`);
        resolve('Link not exists.');
      } else {
        console.log(`get_link_by_uid: OK`);
        resolve(root_path + '/users/' + row.link);
      }
    });
  });
}
async function get_row_by_link(link) {
  return await new Promise((resolve, reject) => {
    db.get(`SELECT    lat, lng, datetime, name, email
                      FROM locations
                      WHERE link = ?`, [link], (err, row) => {
      if (err) {
        reject(err);
        return console.error(err.message);
      }
      if (typeof row === 'undefined') {
        // console.log(`get_row_by_link: NOT FOUND`);
        resolve(false);
      } else {
        // console.log(`get_row_by_link: OK`);
        resolve(row);
      }
    });
  });
}

module.exports.insert_location = insert_location;
module.exports.update_location_pos = update_location_pos;
module.exports.update_location_link = update_location_link;
module.exports.is_link = is_link;
module.exports.uid_is_exists = uid_is_exists;
module.exports.new_link = new_link;
module.exports.get_full_link_by_uid = get_full_link_by_uid;
module.exports.get_row_by_link = get_row_by_link;