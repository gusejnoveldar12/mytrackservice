/**
 * Module dependencies.
 */

var date_to_string = (require('../bin/var')).date_to_string;
var database = require('../sqlite3/database');
var fb_admin = require('../auth/firebase-admin.js');
var app = require('../app');
var debug = require('debug')('myapp:server');
var http = require('http');
var websocket_server = require("websocket").server;

var fs = require('fs');
var https = require('https');
var privateKey  = fs.readFileSync('ssl/server.key', 'utf8');
var certificate = fs.readFileSync('ssl/server.crt', 'utf8');
var credentials = {key: privateKey, cert: certificate};

/**
 * Get port from environment and store in Express.
 */

var port = normalizePort('3000');
app.set('port', port);

var port_https = normalizePort('3443');
app.set('port_https', port_https);

/**
 * Create HTTP server.
 */

var server = http.createServer(app);

var server_https = https.createServer(credentials, app);

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

// server_https.listen(port_https);
// server_https.on('error', onError_https);
// server_https.on('listening', onListening_https);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  var port = parseInt(val, 10);
  if (isNaN(port)) {
    // named pipe
    return val;
  }
  if (port >= 0) {
    // port number
    return port;
  }
  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

function onError_https(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port_https === 'string'
    ? 'Pipe ' + port_https
    : 'Port ' + port_https;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
}

function onListening_https() {
  var addr = server_https.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
}

/**
 * Websocket_server
 */

var ws = new websocket_server({httpServer: server, autoAcceptConnections: false});

function requestIsAllowed(req) {
  var isAllowed = false;
  if (req.resource == '/api') return true;
  if (req.resource.slice(0, 6) == '/users') return true;
  return isAllowed;
}

function requestIsApi(req) {
  return ((typeof req.origin === 'undefined')
          & (req.resource == '/api'));
}

ws.on('request', function(request) {
  if (!requestIsAllowed(request)) {
    request.reject();
    console.log(date_to_string(Date.now()) + ' Connection from origin ' + request.origin + ' rejected.');
    return;
  }
  var connection = request.accept(null, request.origin);
  console.log(date_to_string(Date.now()) + ' Connection accepted.');
  if (requestIsApi(request)) {
    connection.on('message', function(message) {
      if (message.type === 'utf8') {
        try {
          var array = JSON.parse(message.utf8Data);
          // console.log('JSON parse: OK');
          fb_admin.auth().verifyIdToken(array[0])
          .then(async function(decodedToken) {
            if (array[1] == 'MYLOCATION') {
              if (await database.uid_is_exists(decodedToken.uid)) {
                database.update_location_pos(decodedToken.uid, array[2], array[3]);
              } else {
                database.insert_location(decodedToken.uid, await database.new_link(), array[2], array[3], decodedToken.name, decodedToken.email);
              }
            } else if (array[1] == 'GETLINK') {
              var link = await database.get_full_link_by_uid(decodedToken.uid);
              console.log('LINK='+link);
              connection.sendUTF(link);
            }
            // console.log('verifyIdToken: OK');
          }).catch(function(error) {
            console.error(error.message);
          });
        } catch (error) {
          console.error(error.message);
        }
        //console.log('Received Message: ' + message.utf8Data);
        //connection.sendUTF(message.utf8Data);
      }
    });
  } else {
    let timerId = setTimeout(async function tick() {
      if (!connection.connected) {
        connection.close(1007, 'Row not found.');
        return;
      }
      var row = await database.get_row_by_link(request.resource.slice(7));
      if ((row !== false)&&(connection.connected)) {
        var msg = '['+row.lat+','+row.lng+','+row.datetime+']';
        connection.sendUTF(msg);
        console.log(date_to_string(Date.now())+' Send Message: '+msg);
        timerId = setTimeout(tick, 5000); // (*)
      } else {
        connection.close(1007, 'Row not found.');
      }
    }, 5000);
  }
  connection.on('close', function(reasonCode, description) {
      console.log(date_to_string(Date.now()) + ' Peer ' + connection.remoteAddress + ' disconnected.');
  });
});