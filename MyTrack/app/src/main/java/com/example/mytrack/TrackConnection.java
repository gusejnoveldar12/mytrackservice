package com.example.mytrack;

import android.Manifest;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;


import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GetTokenResult;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.WebSocket;

public class TrackConnection extends Service {
    AlarmManager mAlarmManager;
    public static final String CHANNEL_ID = "ForegroundServiceChannel";
    WifiManager mWifiManager;
    WifiManager.WifiLock mWifiManagerLock;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        mWifiManager = (WifiManager)this.getSystemService(Context.WIFI_SERVICE);;
        mWifiManagerLock = null;


        mWifiManagerLock = mWifiManager.createWifiLock(WifiManager.WIFI_MODE_FULL, "MyWFL");
        mWifiManagerLock.acquire();
    }

    @Override
    public void onDestroy() {
        mWifiManagerLock.release();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            stopSelf(startId);
            return START_STICKY;
        }


        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            stopSelf(startId);
            return START_STICKY;
        }


        //Toast.makeText(this, "Service working", Toast.LENGTH_SHORT).show();


        String input = intent.getStringExtra("inputExtra");
        createNotificationChannel();


        Intent ai = new Intent(this.getApplicationContext(), MainActivity.class);
        PendingIntent pai = PendingIntent.getActivity(this.getApplicationContext(), 0, ai, 0);

        Notification notification = new NotificationCompat.Builder(this.getApplicationContext(), CHANNEL_ID)
                .setContentTitle("Внимание, ваша геолокация отправляется на сервер")
                .setContentText(input)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentIntent(pai)
                .build();

        Intent alarmai = new Intent("START_TRACK_CONNECTION");
        alarmai.setComponent(new ComponentName(this, TrackConnectionLauncher.class));
        PendingIntent alarmpai = PendingIntent.getBroadcast(this, 1, alarmai, 0);
        mAlarmManager = (AlarmManager)getSystemService(this.getApplicationContext().ALARM_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mAlarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP,System.currentTimeMillis()+60000, alarmpai);
        }
        else {
            mAlarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP,System.currentTimeMillis()+5000, alarmpai);
        }

        if (startId == 1)
            startForeground(1, notification);



        FirebaseAuth.getInstance().getCurrentUser().getIdToken(true).addOnCompleteListener(new sendTrackComplete(this, startId));

        return START_STICKY;
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Foreground Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }




    class sendTrackComplete implements OnCompleteListener<GetTokenResult> {
        Context context;
        MyLocationListener mls;
        int startId;


        sendTrackComplete(Context context, int starId) {
            this.context = context;
            this.startId = starId;
        }


        @Override
        public void onComplete(@NonNull Task<GetTokenResult> task) {
            if (task.isSuccessful()) {
                Request request = new Request.Builder().url("wss://MyTrackApplication--a4elru16.repl.co/api").build();
                MyWebSocketListener myWebSocketListener = new MyWebSocketListener(context);;
                OkHttpClient client = new OkHttpClient();
                WebSocket ws = client.newWebSocket(request, myWebSocketListener);
                client.dispatcher().executorService().shutdown();

                String token = task.getResult().getToken();

                mls.SetUpLocationListener(context);



                if (mls.imHere == null) {
                    Log.d("GPS", "Координаты не определены и не будут отправлены");
                    Toast.makeText(context, "GPS координаты не определены и не будут отправлены", Toast.LENGTH_SHORT).show();
                    //ws.send("[\"" + token + "\", \"MYLOCATION\", \"NULL\", \"NULL\"]");
                }

                else {
                    Log.d("GPS", "Широта: " + mls.imHere.getLatitude() + "  Долгота:" + mls.imHere.getLongitude());
                    //Toast.makeText(context,  "Широта: " + mls.imHere.getLatitude() + "  Долгота:" + mls.imHere.getLongitude(), Toast.LENGTH_SHORT).show();
                    ws.send("[\"" + token + "\", \"MYLOCATION\", \"" + mls.imHere.getLatitude() + "\", \"" + mls.imHere.getLongitude() + "\"]");
                }


                ws.close(MyWebSocketListener.NORMAL_CLOSURE_STATUS, "GOODBYE");
            }
        }
    }
}


